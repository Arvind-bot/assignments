async function getUserAsync() 
{
    var url='https://reqres.in/api/users?page=2';
  const response = await fetch(url);
    const data = await response.json();
    return data.data;
}

function setPage(){
    getUserAsync()
  .then(data => setNavBar(data));
}

function setNavBar(data){
    var element=document.getElementById('selectId');
    
    for(let i=0;i<data.length;i++){
        var child=document.createElement('option')
        child.setAttribute('id',data[i].id);
        child.innerHTML='Employee ID '+data[i].id;
        element.appendChild(child);
    }
}

function selectedId(){
    var id=event.srcElement[event.srcElement.selectedIndex].id;
    getUserAsync()
  .then(data => performAction(data,id));
}


function createUserDetailsNode(data,id){
    var userDetails=document.createElement('div');
    userDetails.setAttribute('id','userDetails');
    // console.log(userDetails);
    var emid=document.createElement('p');
    emid.innerHTML='Employee ID : '+data[id].id;
    var email=document.createElement('p');
    email.innerHTML='Email : '+data[id].email;
    var fn=document.createElement('p');
    fn.innerHTML='First Name : '+data[id].first_name;
    var ln=document.createElement('p');
    ln.innerHTML='Last Name : '+data[id].last_name;
    userDetails.appendChild(emid);
    userDetails.appendChild(email);
    userDetails.appendChild(fn);
    userDetails.appendChild(ln);
    return userDetails;
}

function createUserImageNode(url){
    var userImage=document.createElement('div');
    userImage.setAttribute('id','userImage');
    // console.log(userImage);
    var imageNode=document.createElement('img');
    imageNode.setAttribute('src',url);
    userImage.appendChild(imageNode);
    return userImage;
}

function createUserInfoNode(data,id){
    var completeUserInformation=document.createElement('div');
    completeUserInformation.setAttribute('id','completeUserInfo');
    // console.log(completeUserInformation);
    completeUserInformation.appendChild(createUserDetailsNode(data,id));
    completeUserInformation.appendChild(createUserImageNode(data[id].avatar));
    // console.log(completeUserInformation);
    return completeUserInformation;
}

function createSingleUser(data, id){
    let rid=0;
    for(rid=0;rid<data.length;rid++){
        if(data[rid].id==id)
            break;
    }
    document.getElementById('userInformation').innerHTML='';
    var userInfoNode=createUserInfoNode(data,rid);
    // document.getElementById('userInformation').appendChild(userInfoNode);
    
    document.getElementById('userInformation').appendChild(userInfoNode);
    // console.log(document.getElementById('userInformation'));

}

function createAllUsers(data){
    document.getElementById('userInformation').innerHTML='';
    
    for(var i=0;i<data.length;i++){
        document.getElementById('userInformation').appendChild(createUserInfoNode(data,i));
        document.getElementById('userInformation').appendChild(document.createElement('br'));
        document.getElementById('userInformation').appendChild(document.createElement('br'));
    }
    // console.log(document.getElementById('userInformation'));
}

function performAction(data,id){
    
    if(id=='selectOption'){
        var para=document.createElement('p');
        para.innerHTML='Select option to display user information.';
        document.getElementById('userInformation').innerHTML='';
        document.getElementById('userInformation').appendChild(para);
    }else if(id=='allUsers'){
        createAllUsers(data); 
    }else{
       createSingleUser(data,id);
    };
}

setPage();