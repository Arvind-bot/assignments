
    
    function clearall(){
        document.getElementById("bmiform").reset();
        document.getElementById("bmi-result").innerHTML="";

    }

    function check(){
        if(localStorage.getItem("data")==null){
            localStorage.setItem("data","[]");
        }
    }

    function getFavFoods(favFoods){
        var favFoodsList="";
        var len=favFoods.length-1;
        for(var j=0;j<=len;j++){
            if(favFoods[j]==1){
                favFoodsList+="Pizza";
            }else if(favFoods[j]==2){
                favFoodsList+="Biryani"; 
            }else if(favFoods[j]==3){
                favFoodsList+="Fast-Food";
            }

            if(j<len){
                favFoodsList+=", ";
            }else{
                favFoodsList+=".";
            }
        }
        return favFoodsList
    }

    function loadRecords(){
        localStorage.setItem("edit","true");
        var data=JSON.parse(localStorage.getItem("data"));
        for(var i=0;i<data.length;i++){
            // var obj=localStorage.getItem(localStorage.key(i));
            // var objParsed=JSON.parse(obj);
            var temp=document.getElementsByTagName("template")[0];
            var clone = temp.content.cloneNode(true);
            
            clone.querySelector("#name").textContent=data[i].Name;
            clone.querySelector("#age").textContent=data[i].Age;
            clone.querySelector("#gender").textContent=data[i].Gender;
            clone.querySelector("#weight").textContent=data[i].Weight;
            clone.querySelector("#height").textContent=data[i].Height;
            var favFoods=data[i].favfoods;
            var favFoodsList="";
            favFoodsList=getFavFoods(favFoods);
            clone.querySelector("#favorite-foods").textContent=favFoodsList;
            console.log(data[i].BMI);
            clone.querySelector("#bmi-text").textContent=data[i].BMI;
            // clone.querySelector("tr").setAttribute("id",data[i].ID);
            document.getElementById("records-table").appendChild(clone);
            // console.log(document.getElementById("records-table").children[i].rowIndex);
        }
        
        
        var clearRecordButton=document.createElement("button");
        clearRecordButton.innerHTML="Clear All Records<span class=\"fas fa-window-close\" id=\"clear-all-records-btn-span\"></span>";
        clearRecordButton.setAttribute("onclick","clearRecords()");
        clearRecordButton.setAttribute("class","btn btn-danger clear-records-btn-styling");
        clearRecordButton.setAttribute("id","clear-all-records-btn");
        // var out="<button class=\"btn btn-danger\" onclick=\"clearRecords()\">Clear Records</button>";
        if(data.length>1){
            var ele=document.getElementById("content");
            ele.appendChild(clearRecordButton);
        }
        
    }


    function clearRecords(){
        document.getElementById("records-table").innerHTML="";
        document.getElementById("content").innerHTML="";
        localStorage.setItem("data","[]");
    }

    function deleteRecord(obj){
        var data=JSON.parse(localStorage.getItem("data"));
        var index=obj.parentNode.parentNode.rowIndex-1;
        // console.log(obj.parentNode.parentNode.rowIndex);
        document.getElementById("records-table").deleteRow(index);
        data.splice(index,1);
        if(data.length<=1)
        document.getElementById("content").innerHTML="";
        localStorage.data=JSON.stringify(data);
    }

    function checkNameIsValid(obj){
        $('[id="name"]').tooltip("dispose");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("data-toggle","tooltip");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("data-placement","bottom");
        
        $('[id="name"]').tooltip('hide');
        if(obj.parentNode.parentNode.querySelector("#name").innerHTML==""){
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#name").setAttribute("data-original-title","Name cannot be empty.");
            $('[id="name"]').tooltip('show');
        }else if(obj.parentNode.parentNode.querySelector("#name").innerHTML.match(/^[A-Za-z]+$/)==null){
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#name").setAttribute("data-original-title","Name cannot have characters other than alphabets.");
            $('[id="name"]').tooltip('show');
        }else{
            obj.parentNode.parentNode.querySelector("#name").setAttribute("data-original-title","");
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-no-error");
        }
    }

    function checkAgeIsValid(obj){
        var age=obj.parentNode.parentNode.querySelector("#age");
        $('[id="age"]').tooltip("dispose");
        age.setAttribute("data-toggle","tooltip");
        age.setAttribute("data-placement","top");
        
        $('[id="age"]').tooltip('hide');
        if(age.innerHTML==""){
            age.setAttribute("class","when-error");
            age.setAttribute("data-original-title","Age cannot be empty.");
            $('[id="age"]').tooltip('show');
        }else if(obj.parentNode.parentNode.querySelector("#age").innerHTML.match(/^[0-9]+$/)==null){
            age.setAttribute("class","when-error");
            age.setAttribute("data-original-title","Age cannot have characters other than numbers.");
            $('[id="age"]').tooltip('show');
        }else if(parseInt(age.innerHTML)<1 || parseInt(age.innerHTML)>130){
            age.setAttribute("class","when-error");
            age.setAttribute("data-original-title","Age should be in between 1-130 years.");
            $('[id="age"]').tooltip('show');
        }else{
            age.setAttribute("class","when-no-error");
            age.setAttribute("data-original-title","");
        }
    }

    
    

    // function checkNameIsValid(obj){
    //     $('[data-toggle="tooltip"]').tooltip("dispose");
    //     $('[data-toggle="tooltip"]').tooltip('hide');
    //     if(obj.parentNode.parentNode.querySelector("#name").innerHTML==""){
    //         $('[data-toggle="tooltip"]').tooltip('show');
    //         obj.parentNode.parentNode.querySelector("#name").setAttribute("title","Name could not have characters other than alphabets.");
    //     }
    // }

    function update(obj){
        
        var name=obj.parentNode.parentNode.querySelectorAll("td")[0].innerHTML;
        var age=obj.parentNode.parentNode.querySelectorAll("td")[1].innerHTML;

        var gender=obj.parentNode.parentNode.querySelectorAll("td")[2].querySelector("#gender").value;
        var weight=obj.parentNode.parentNode.querySelectorAll("td")[3].innerHTML;
        var height=obj.parentNode.parentNode.querySelectorAll("td")[4].innerHTML;
        var bmi=""+Math.round(weight/Math.pow(height/100,2));
        var elid=obj.parentNode.parentNode.rowIndex-1;
        // var elid=obj.parentNode.parentNode.getAttribute("id");
        var ffop="";
        favFoodsList=[];
        var favFoods=obj.parentNode.parentNode.querySelector("#fav-foods").querySelectorAll("input");
        // console.log(favFoods);
        var isFavFoodsOk=false;
        for(var j=0;j<favFoods.length;j++){
            if(favFoods[j].checked){
                isFavFoodsOk=true;
                favFoodsList.push(j+1);
            }    
        }
        
        var isOk=true;
            if(name==""){
                // $('[data-toggle="tooltip"]').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-error");
                // obj.parentNode.parentNode.querySelector("#name").setAttribute("data-toggle","tooltip");
                // obj.parentNode.parentNode.querySelector("#name").setAttribute("data-placement","bottom");
                // obj.parentNode.parentNode.querySelector("#name").setAttribute("title","Name could not be empty.");
                // // console.log(obj.parentNode.parentNode.querySelector("#name"));
                obj.parentNode.parentNode.querySelector("#name").setAttribute("data-original-title","Name could not be empty.");
                $('#name').tooltip('show');
                isOk=false;    
            }else if(name.match(/^[A-Za-z]+$/)==null){
                obj.parentNode.parentNode.querySelector("#name").setAttribute("data-original-title","Name cannot have characters other than alphabets.");
                $('#name').tooltip('show');
                isOk=false;            
            }else{
                $('#name').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("class");
                obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-no-error");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("data-toggle");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("data-placement");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("title");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("data-original-title");
                obj.parentNode.parentNode.querySelector("#name").removeAttribute("aria-describedby");
                // console.log(obj.parentNode.parentNode.querySelector("#name"));
                
            }

            if(age==""){
                obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-error");
                // innerHTML="*Age could not be empty.";
                obj.parentNode.parentNode.querySelector("#age").setAttribute("data-original-title","Age cannot be empty.");
                $('#age').tooltip('show');
                isOk=false;
            }else if(age.match(/^[0-9]+$/)==null){
                obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#age").setAttribute("data-original-title","Age cannot have characters other than numbers.");
                $('#age').tooltip('show');
                // innerHTML="*Age could not contain charecters other than numbers.";
                isOk=false;
            }else if(age<1 || age>150){
                obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelectorAll("#age").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#age").setAttribute("data-original-title","Age should be in between 1-130 years.");
                $('#age').tooltip('show');
                // innerHTML="*Age could not be greater than 150.";
                isOk=false;
            }else{
                $('#age').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("aria-describedby");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("class");
                obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-no-error");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("data-toggle");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("data-placement");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("title");
                obj.parentNode.parentNode.querySelector("#age").removeAttribute("data-original-title");
            }
    
            if(weight==""){
                // $('#weight').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("data-original-title","Weight cannot be empty.");
                // // console.log(obj.parentNode.parentNode.querySelector("#name"));
                // $('#weight').tooltip('show');
                // obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                // obj.parentNode.parentNode.querySelector("#weight").setAttribute("data-placement","left");
                // // innerHTML="*Age could not be empty.";
                // obj.parentNode.parentNode.querySelector("#weight").setAttribute("data-original-title","Weight cannot be empty.");
                $('#weight').tooltip('show');
                isOk=false;
            }else if(weight.match(/^[0-9]+$/)==null){
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("data-original-title","Weight cannot have characters other than numbers.");
                $('#weight').tooltip('show');
                // innerHTML="*Age could not contain charecters other than numbers.";
                isOk=false;
            }else if(weight<3 || weight>400){
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("data-original-title","Weight should be in between 3-500 kgs.");
                $('#weight').tooltip('show');
                // innerHTML="*Age could not be greater than 150.";
                isOk=false; 
            }else{
                $('#weight').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("aria-describedby");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("class");
                obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-no-error");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("data-toggle");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("data-placement");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("title");
                obj.parentNode.parentNode.querySelector("#weight").removeAttribute("data-original-title");
                // document.getElementById("age-error-message").innerHTML="";
            }
    
            if(height==""){
                obj.parentNode.parentNode.querySelector("#height").setAttribute("class","when-error");
                // innerHTML="*Age could not be empty.";
                obj.parentNode.parentNode.querySelector("#height").setAttribute("data-original-title","Height cannot be empty.");
                $('#height').tooltip('show');
                isOk=false;
            }else if(height.match(/^[0-9]+$/)==null){
                obj.parentNode.parentNode.querySelector("#height").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#height").setAttribute("data-original-title","Height cannot have characters other than numbers.");
                $('#height').tooltip('show');
                // innerHTML="*Age could not contain charecters other than numbers.";
                isOk=false;
            }else if(height<24 || height>250){
                obj.parentNode.parentNode.querySelector("#height").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#height").setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#height").setAttribute("data-original-title","Height should be in between 24-250 cms.");
                $('#height').tooltip('show');
                // innerHTML="*Age could not be greater than 150.";
                isOk=false; 
            }else{
                $('#height').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("aria-describedby");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("class");
                obj.parentNode.parentNode.querySelector("#height").setAttribute("class","when-no-error");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("data-toggle");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("data-placement");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("title");
                obj.parentNode.parentNode.querySelector("#height").removeAttribute("data-original-title");
                // document.getElementById("age-error-message").innerHTML="";
            }
    
            if(favFoodsList.length==0){
                obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","when-error");
                // innerHTML="*Age could not be empty.";
                obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("data-original-title","Please select atleast one option.");
                $('#favorite-foods').tooltip('show');
                isOk=false;
            }else{
                $('#favorite-foods').tooltip("dispose");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("aria-describedby");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("class");
                obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","when-no-error");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-toggle");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-placement");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("title");
                obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-original-title");
            }
            
        

        if(isOk==false)
            return;
        
            // $(obj.parentNode.parentNode).tooltip("dispose");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("aria-describedby");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("class");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","when-no-error");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-toggle");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-placement");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("title");
            // obj.parentNode.parentNode.querySelector("#favorite-foods").removeAttribute("data-original-title");

        localStorage.setItem("edit","true");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#gender").innerHTML=gender;
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("contenteditable","false");
        // var ffop="";
        // favFoodsList=[];
        
        // var favFoods=obj.parentNode.parentNode.querySelector("#fav-foods").querySelectorAll("input");
        // console.log(favFoods);
        // for(var j=0;j<favFoods.length;j++){
        //     if(favFoods[j].checked){
        //         favFoodsList.push(j+1);
        //     }    
        // }
        // console.log(favFoodsList);
        var len=favFoodsList.length-1;
        
        for(var j=0;j<=len;j++){
            if(favFoodsList[j]==1){
                ffop+="Pizza"; 
            }else if(favFoodsList[j]==2){
                ffop+="Biryani";
            }else if(favFoodsList[j]==3){
                ffop+="Fast-Food";
            }

            if(j<len)
                ffop+=", ";
            else
                ffop+=".";
        }
        // console.log(ffop);
        obj.parentNode.parentNode.querySelector("#favorite-foods").innerHTML=ffop;
        obj.parentNode.parentNode.querySelector("#bmi-text").innerHTML=bmi;
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:inline");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#gender").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","");

        var out1="<button class=\"btn-primary edit-btn\" onclick=\"editRecord(this)\" id=\"b\"><span class=\"fas fa-edit\" style=\"font-weight: 100;margin-right: 4px;\"></span>Edit</button>"
        var out2="<button class=\"btn delete-btn\" onclick=\"deleteRecord(this)\" id=\"b\" ><span class=\"fas fa-trash-alt\" style=\"font-weight: 100;margin-right: 4px;\"></span>Delete</button>"
        
        obj.parentNode.parentNode.querySelector("#btn2").innerHTML=out2;
        obj.parentNode.parentNode.querySelector("#btn1").innerHTML=out1;

        // var updateObj=localStorage.getItem(elid);
        // var parsedUpdateObj=JSON.parse(updateObj);
        
        // parsedUpdateObj.Name=name;
        // parsedUpdateObj.Age=age;
        // parsedUpdateObj.Gender=gender;
        // parsedUpdateObj.Weight=weight;
        // parsedUpdateObj.Height=height;
        // parsedUpdateObj.BMI=bmi;

        var data=JSON.parse(localStorage.getItem("data"));
        data[elid].Name=name;
        data[elid].Age=age;
        data[elid].Gender=gender;
        data[elid].Weight=weight;
        data[elid].Height=height;
        data[elid].favfoods=favFoodsList;
        data[elid].BMI=bmi;

        localStorage.data=JSON.stringify(data);
        
    }

    function cancel(obj){
        localStorage.setItem("edit","true");
        var elid=obj.parentNode.parentNode.rowIndex-1;
        // console.log(elid);
        // console.log(obj);
        // console.log(elid);
        // console.log(JSON.stringify(elid));
        // console.log(localStorage);
        // var stringifiedElid=elid.toString();
        // var tempObj=localStorage.getItem(stringifiedElid);
        // console.log(tempObj);
        // var objParsed=JSON.parse(tempObj);
        // console.log(objParsed);

        var data=JSON.parse(localStorage.getItem("data"));
        // console.log(data);
        obj.parentNode.parentNode.querySelector("#name").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#gender").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:inine");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#gender").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("class","");
        obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","");
        // console.log(data[elid]);
        obj.parentNode.parentNode.querySelector("#name").innerHTML=data[elid].Name;
        obj.parentNode.parentNode.querySelector("#age").innerHTML=data[elid].Age;
        obj.parentNode.parentNode.querySelector("#gender").innerHTML=data[elid].Gender;
        obj.parentNode.parentNode.querySelector("#weight").innerHTML=data[elid].Weight;
        obj.parentNode.parentNode.querySelector("#height").innerHTML=data[elid].Height;
        var favFoods=data[elid].favfoods;
        var favFoodsList="";
        favFoodsList=getFavFoods(favFoods);
        obj.parentNode.parentNode.querySelector("#favorite-foods").innerHTML=favFoodsList;

        var out1="<button class=\"btn-primary edit-btn\" onclick=\"editRecord(this)\" id=\"b\"><span class=\"fas fa-edit\" style=\"font-weight: 100;margin-right: 4px;\"></span>Edit</button>"
        var out2="<button class=\"btn delete-btn\" onclick=\"deleteRecord(this)\" id=\"b\" ><span class=\"fas fa-trash-alt\" style=\"font-weight: 100;margin-right: 4px;\"></span>Delete</button>"
        obj.parentNode.parentNode.querySelector("#btn1").innerHTML=out1;
        obj.parentNode.parentNode.querySelector("#btn2").innerHTML=out2;
        
    }

    function checkIsLengthValid(obj){
        // alert($(obj).text().length);
        $('[id="age"]').tooltip("dispose");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("data-toggle","tooltip");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("data-placement","bottom");
        // obj.parentNode.parentNode.querySelector("#age").setAttribute("title","");
        $('[id="age"]').tooltip('hide');
        if($(obj).text().length === 3 && event.keyCode != 8) {
            event.preventDefault();
        }

    }
    
    function checkWeightIsValid(obj){
        var weight=obj.parentNode.parentNode.querySelector("#weight");
        $('[id="age"]').tooltip("dispose");
        weight.setAttribute("data-toggle","tooltip");
        weight.setAttribute("data-placement","top");
        
        $('[id="weight"]').tooltip('hide');
        if(weight.innerHTML==""){
            weight.setAttribute("class","when-error");
            weight.setAttribute("data-original-title","weight cannot be empty.");
            $('[id="weight"]').tooltip('show');
        }else if(weight.innerHTML.match(/^[0-9]+$/)==null){
            weight.setAttribute("class","when-error");
            weight.setAttribute("data-original-title","Weight cannot have characters other than numbers.");
            $('[id="weight"]').tooltip('show');
        }else if(parseInt(weight.innerHTML)<3 || parseInt(weight.innerHTML)>500){
            weight.setAttribute("class","when-error");
            weight.setAttribute("data-original-title","Weight should be in between 3-500 kgs.");
            $('[id="weight"]').tooltip('show');
        }else{
            weight.setAttribute("class","when-no-error");
            weight.setAttribute("data-original-title","");
        }    
    }

    function checkHeightIsValid(obj){
        var height=obj.parentNode.parentNode.querySelector("#height");
        $('[id="age"]').tooltip("dispose");
        height.setAttribute("data-toggle","tooltip");
        height.setAttribute("data-placement","bottom");
        
        $('[id="height"]').tooltip('hide');
        if(height.innerHTML==""){
            height.setAttribute("class","when-error");
            height.setAttribute("data-original-title","Height cannot be empty.");
            $('[id="height"]').tooltip('show');
        }else if(height.innerHTML.match(/^[0-9]+$/)==null){
            weight.setAttribute("class","when-error");
            weight.setAttribute("data-original-title","Height cannot have characters other than numbers.");
            $('[id="height"]').tooltip('show');
        }else if(parseInt(height.innerHTML)<24 || parseInt(height.innerHTML)>250){
            height.setAttribute("class","when-error");
            height.setAttribute("data-original-title","height should be in between 24-250 cms.");
            $('[id="height"]').tooltip('show');
        }else{
            height.setAttribute("class","when-no-error");
            height.setAttribute("data-original-title","");
        }    
    }

    
    function editRecord(obj){
        if(localStorage.getItem("edit")=="true"){
            localStorage.setItem("edit","false");
        }else{
            alert("update or cancel current record first.");
            return;
        }
        obj.parentNode.parentNode.querySelector("#name").setAttribute("contenteditable","true");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("contenteditable","true");
        console.log(obj.parentNode.parentNode.querySelector("#age"));
        // obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("contenteditable","true");
        var curVal=obj.parentNode.parentNode.querySelector("#gender").innerHTML;
        // console
        var temp=document.getElementById("gender-template");
        
        // console.log(temp.content);
        var clone = temp.content.cloneNode(true);
        if(clone.getElementById("male").value==curVal){
            clone.getElementById("male").selected=true;
        }else if(temp.content.getElementById("female").value==curVal){
            clone.getElementById("female").selected=true;
        }

        // for(var i=0;i<;i++ ){
        //     obj.parentNode.parentNode.querySelector(i).innerHTML="";
        // }
        // clone.getElementById("male").s=true
        // console.log(clone.getElementById("male"));
        // console.log(clone.getElementById("female"));
        obj.parentNode.parentNode.querySelector("#gender").innerHTML="";
        obj.parentNode.parentNode.querySelector("#gender").appendChild(clone);
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("contenteditable","true");
        obj.parentNode.parentNode.querySelector("#Height").setAttribute("contenteditable","true");
        
        var temp=document.getElementById("favfoods-template");
        // console.log(temp.content);
        var clone = temp.content.cloneNode(true);

        obj.parentNode.parentNode.querySelector("#favorite-foods").innerHTML="";
        obj.parentNode.parentNode.querySelector("#favorite-foods").appendChild(clone);
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:none;");
        obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#gender").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#Height").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","when-editing");

        obj.parentNode.parentNode.querySelector("#name").setAttribute("onkeyup","checkNameIsValid(this)");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("onkeyup","checkAgeIsValid(this)");
        obj.parentNode.parentNode.querySelector("#age").setAttribute("onkeydown","checkIsLengthValid(this)");
        // obj.parentNode.parentNode.querySelector("#gender").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("onkeyup","checkWeightIsValid(this)");
        obj.parentNode.parentNode.querySelector("#weight").setAttribute("onkeydown","checkIsLengthValid(this)");
        // obj.parentNode.parentNode.querySelector("#Height").setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("onkeyup","checkHeightIsValid(this)");
        obj.parentNode.parentNode.querySelector("#height").setAttribute("onkeydown","checkIsLengthValid(this)");
        // obj.parentNode.parentNode.querySelector("#favorite-foods").setAttribute("class","when-editing");

        var out1="<button class=\"btn-success\" id=\"b\" onclick=\"update(this)\" ><span class=\"fas fa-sync\" style=\"font-size: 14px; margin-right: 4px;\"></span>Update</button>";
        var out2="<button class=\"btn-danger\" id=\"b\" onclick=\"cancel(this)\" ><span class=\"far fa-times-circle\" style=\"font-size: 14px; margin-right: 4px;\"></span>Cancel</button>";
        var elid=obj.parentNode.parentNode.getAttribute("id");

        var tag = obj.parentNode.parentNode.cells[0]; 
            var l=tag.childNodes[0].length;
            // Creates range object 
            var setpos = document.createRange(); 
              
            // Creates object for selection 
            var set = window.getSelection(); 
              
            // Set start position of range 
            setpos.setStart(tag.childNodes[0], l); 
              
            // Collapse range within its boundary points 
            // Returns boolean 
            setpos.collapse(true); 
              
            // Remove all ranges set 
            set.removeAllRanges(); 
              
            // Add range with respect to range object. 
            set.addRange(setpos); 
              
            // Set cursor on focus 
            tag.focus();

        obj.parentNode.parentNode.querySelector("#btn2").innerHTML=out2;
        obj.parentNode.parentNode.querySelector("#btn1").innerHTML=out1;
        
        
    }

    function validate(name, age, gender,weight, height, isFavFoodsOk){
        var isOk=true;
        if(name==""){
            document.getElementById("name-error-message").innerHTML="*Name could not be empty.";
            isOk=false;
            
        }else if(name.match(/^[A-Za-z]+$/)==null){
            document.getElementById("name-error-message").innerHTML="*Name could not have charecters other than alphabets.";
            isOk=false;            
        }else{
            document.getElementById("name-error-message").innerHTML="";
        }

        if(age==""){
            document.getElementById("age-error-message").innerHTML="*Age could not be empty.";
            isOk=false;
        }else if(age>150){
            document.getElementById("age-error-message").innerHTML="*Age could not be greater than 150.";
            isOk=false;
        }else if(age.match(/^[0-9]+$/)==null){
            document.getElementById("age-error-message").innerHTML="*Age could not contain charecters other than numbers.";
            isOk=false;
        }else{
            document.getElementById("age-error-message").innerHTML="";
        }

        if(gender==null){
            document.getElementById("gender-error-message").innerHTML="*Please select the gender.";
            isOk=false;
        }else{
            document.getElementById("gender-error-message").innerHTML="";
        }

        if(weight==""){
            document.getElementById("weight-error-message").innerHTML="*weight could not be empty.";
            isOk=false;
        }else if(weight.match(/^[0-9]+$/)==null){
            document.getElementById("weight-error-message").innerHTML="*Weight could not contain charecters other than numbers.";
            isOk=false;
        }else if(weight<3 || weight>500){
            document.getElementById("weight-error-message").innerHTML="*Enter a valid weight in kg.";
            isOk=false;
        }else{
            document.getElementById("weight-error-message").innerHTML="";
        }

        if(height==""){
            document.getElementById("height-error-message").innerHTML="*height could not be empty.";
            isOk=false;
        }else if(height<24 || height>250){
            document.getElementById("height-error-message").innerHTML="*Enter a valid height in cm.";
            isOk=false;
        }else if(height.match(/^[0-9]+$/)==null){
            document.getElementById("height-error-message").innerHTML="*Age could not contain charecters other than numbers.";
            isOk=false;
        }else{
            document.getElementById("height-error-message").innerHTML="";
        }
        
        if(isFavFoodsOk==false){
            document.getElementById("fav-foods-error-message").innerHTML="*Please select atleast one food item.";
            isOk=false;
        }else{
            document.getElementById("fav-foods-error-message").innerHTML="";
        }

        return isOk;
    }


    function calculateBMI(){
        if(localStorage.getItem("data")==null){
            // localStorage.setItem("record-counter","0");
            localStorage.setItem("data","[]");
        }
        
        var name=document.getElementById("name").value;
        var age=document.getElementById("age").value;
        var rb1=document.getElementById("male-radio-button");
        var rb2=document.getElementById("female-radio-button");
        var gender;
        if(rb1.checked){
            gender=rb1.value;
        }else if(rb2.checked){
            gender=rb2.value;
        }
        var weight=document.getElementById("weight").value;
        var height=document.getElementById("height").value;
        var isFavFoodsOk=false;
        var favFoodsList=[];
        var favFoods=document.querySelector("#fav-foods").querySelectorAll("input");
        for(var i=0;i<favFoods.length;i++){
            if(favFoods[i].checked){
                favFoodsList.push(parseInt(favFoods[i].value));
                isFavFoodsOk=true;
            }
        }

        var isOk=validate(name,age,gender,weight,height,isFavFoodsOk);

        if(isOk==false)
            return;

        var bmi=""+Math.round(weight/Math.pow(height/100,2));
        // (document.getElementById("weight").value)/(Math.pow((document.getElementById("height").value/100),2));
        // bmi = Math.round(bmi);
        // bmi=bmi.toString();
        // var rc=localStorage.getItem("record-counter");
        var dataobj=[];
        dataobj=JSON.parse(localStorage.getItem("data"));
        // var irc=parseInt(rc);
        // irc++;
        // console
        // console.log(dataobj);
        // var parsedDataobj=[];
        // console.log(parsedDataobj);
        
        // localStorage.data(JSON.stringify(parsedDataobj));
        // console.log(parsedDataobj);

        // var src=irc.toString();
        // localStorage.setItem("record-counter",src);
        
        // if()
        console.log(favFoodsList);
        var obj={
            // ID:src,
            Name:name,
            Age:age,
            Gender:gender,
            Weight:weight,
            Height:height,
            favfoods:favFoodsList,
            BMI:bmi
        }
 


        dataobj.push(obj);
        // console.log(dataobj);
        localStorage.data=JSON.stringify(dataobj);

        // localStorage.setItem(src,objStringified);
        document.getElementById("bmi-result").innerHTML="Total BMI : "+bmi;

    };


