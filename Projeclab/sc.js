function loadVideo(obj){
    // document.getElementById("modal-video-title").innerHTML="";
    // document.getElementById("modal-video-iframe").setAttribute("src",obj.getAttribute("name"));
    // document.getElementById("modal-video-content").innerHTML="";
    
    var index=$(obj.parentNode.parentNode).index();
    var dataRecord=JSON.parse(localStorage.getItem("videoList"))[index];
    document.getElementById("modal-video-title").innerHTML=dataRecord.videoTitle;
    document.getElementById("modal-video-iframe").setAttribute("src",obj.getAttribute("name"));
    document.getElementById("modal-video-content-title").innerHTML=dataRecord.videoDiscription;
    document.getElementById("modal-video-content-description").innerHTML="description";
}

function search(obj){
    var filter=obj.value.toLowerCase();
    var iv=document.getElementById("videos-row").getElementsByClassName("card-body-customized");
    
    for(var i=0;i<iv.length;i++){
        var txtValue=iv[i].textContent.toLowerCase();
        if(txtValue.indexOf(filter)>-1){
            iv[i].parentNode.parentNode.parentNode.style.display="";
        }else{
            iv[i].parentNode.parentNode.parentNode.style.display="none";
        }
    }
}

// function videoClick(obj){
//     var link=obj.getAttribute("name");
//     var item="video.html?id="+link;
//     location.replace(item);
// }


function truncateString(str, num) {
    if (str.length <= num) {
      return str;
    }
    return str.slice(0, num) + '...';
  }

function loadVideos(){
    if(localStorage.getItem("videosList")==null)
        localStorage.setItem("videoList","[]")
    
    var list=[];
    var names=["https://www.youtube.com/embed/XXfNoUEFjN0","https://www.youtube.com/embed/2SfSUjxNHtE","https://www.youtube.com/embed/UZyHlm7tDPY","https://www.youtube.com/embed/cg1rtWXHSKU","https://www.youtube.com/embed/_6oHIFF97E4","https://www.youtube.com/embed/r2vVsF4LS_I","https://www.youtube.com/embed/ZQpWRenGF_w","https://www.youtube.com/embed/PTpoj4f25dk"];
    var tumbnailLinks=["https://i.ytimg.com/vi/XXfNoUEFjN0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLAdE-4d0rvnFWY0Lq_oGqg5kmHW9A","https://i.ytimg.com/vi/2SfSUjxNHtE/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLDVHxJaK2u2Y4-F71EGaUml6XwDpQ","https://i.ytimg.com/vi/UZyHlm7tDPY/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLByMi8r6kPFJJI44T8ZJG5KbRfMGw","https://i.ytimg.com/vi/cg1rtWXHSKU/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLDz0wl9nf1zDyT-zo9-NufIwXqBEA","https://i.ytimg.com/vi/_6oHIFF97E4/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLBfXzKLe-lH1SfnOJwrp6wtd2E0sw","https://i.ytimg.com/vi/r2vVsF4LS_I/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLCM7M1-V835usqTl7ehVnLAQI2_Eg","https://i.ytimg.com/vi/ZQpWRenGF_w/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLD94wq_cO_pa4AX8In04nCuyUo0wA","https://i.ytimg.com/vi/PTpoj4f25dk/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLAm_jVO8rLzLx8oNzELkaxfSvAEnw"];
    var videoTitles=["The MacBook Pro 14 is Coming!","XPS 13 (2020) Review - Dell Nailed It","Vegeta Completely Annihilates Android 19","Captain America vs Ultron - Fight Scene - Avengers: Age of Ultron - Movie CLIP HD","MORO VS GOHAN! Android 17 Vs 73! A NEW Triple Fusion! Dragon Ball Super Manga Chapter 57 LEAKS","Creating Vision - Captain America vs Tony Stark - Fight Scene - Age of Ultron - Movie CLIP HD","Avengers Vs Guardians Of The Galaxy - Fight Scene - Avengers Infinity War (2018) Movie CLIP HD","Xiaomi Mi Mix Alpha Impressions: The Wraparound Display!"];
    var videoDiscriptions=["video 1","video 2","video 3","video 4","video 5","video 6","video 7","video 8"];

    for(var i=0;i<names.length;i++){
        var videoRecord={
            name:names[i],
            tumbnailLink:tumbnailLinks[i],
            videoTitle:videoTitles[i],
            videoDiscription:videoDiscriptions[i]
        }
        list.push(videoRecord);
    }
    
    localStorage.setItem("videoList",JSON.stringify(list));
    
    var videoList=JSON.parse(localStorage.getItem("videoList"));
    for(var i=0;i<videoList.length;i++){
        var temp=document.getElementsByTagName("template")[0];
        var clone=temp.content.cloneNode(true);
        
        clone.querySelector("#videoLink").setAttribute("name",videoList[i].name);
        clone.querySelector("#tumbnailLink").setAttribute("src",videoList[i].tumbnailLink);
        clone.querySelector("#title").setAttribute("title",videoList[i].videoTitle);
          
        
        clone.querySelector("#title").textContent=truncateString(videoList[i].videoTitle, 43);
        document.getElementById("videos-row").appendChild(clone);

    }

    var modal = document.getElementById('myModal');
    var modalCloseButton=document.getElementById('modalCloseButton');
    var modalCancelButton=document.getElementById('modalCancelButton');
    console.log(modal);

    window.onclick = function (event) {
        var t=event.target;
        if (t == modal || t==modalCloseButton || t==modalCancelButton) {
            document.getElementById("modal-video-title").innerHTML="";
            document.getElementById("modal-video-iframe").removeAttribute("src");
            document.getElementById("modal-video-content-title").innerHTML="";
            document.getElementById("modal-video-content-description").innerHTML="";
        }
        
    }


}